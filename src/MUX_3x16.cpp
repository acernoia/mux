#include <systemc.h>
#include "MUX_3x16.hpp"

using namespace std;

void MUX_3x16::select()
{

   while(true)
   {
     wait();
     switch (sel->read())
     {
       case 0 : Y->write(X0); break;
       case 1 : Y->write(X1); break;
       case 2 : Y->write(X2); break;
       case 3 : Y->write(0); break;
      }
   }

}
