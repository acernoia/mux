#include <systemc.h>
#include "MUX_2x3.hpp"

using namespace std;

void MUX_2x3::select()
{

   while(true)
   {
     wait();
     if (sel->read()==0)
       Y->write(X0);
     else
       Y->write(X1);
   }

}
