#include <systemc.h>
#include <string>
#include "MUX_3x16.hpp"

using namespace std;

SC_MODULE(TestBench)
{
   sc_signal<sc_uint<16> > ingresso0;
   sc_signal<sc_uint<16> > ingresso1;
   sc_signal<sc_uint<16> > ingresso2;
   sc_signal<sc_uint<2> >  selezione;
   sc_signal<sc_uint<16> > uscita;
   MUX_3x16 mux;

   SC_CTOR(TestBench) : mux("mux")
   {
        SC_THREAD(stimulus_thread);
        mux.X0(ingresso0);
        mux.X1(ingresso1);
        mux.X2(ingresso2);
        mux.sel(selezione);
        mux.Y(uscita);
        init_values();
   }

   int check()
   {
        for (unsigned i=0;i<TEST_SIZE;i++) 
        { 
            if (sel_test[i]==3)
            {
                cout << "WARNING (test " << i << " ) : quando l'ingresso di selezione vale tre l'uscita vale sempre zero." << endl;
            }

            if (dato_letto[i] != out_test[i])
            {
                cout << "TEST FALLITO: " << i << endl;
                cout << "RISULTATO TEORICO: " << out_test[i] << endl;
                cout << "RISULTATO TEST :" << dato_letto[i] << endl;
                return 1;
             }
        }
        cout << "TEST OK" << endl;                 
        return 0;
   }

   private:

   void stimulus_thread()
   {
      for (unsigned i=0;i<TEST_SIZE;i++) 
      {
        ingresso0.write(in0_test[i]);
        ingresso1.write(in1_test[i]);
        ingresso2.write(in2_test[i]);
        selezione.write(sel_test[i]);
        cout << "IN0 = " << in0_test[i] << endl;
        cout << "IN1 = " << in1_test[i] << endl;
        cout << "IN2 = " << in2_test[i] << endl;
        cout << "SEL = " << sel_test[i] << endl;
        wait(1,SC_NS);
        dato_letto[i]=uscita.read();
        cout << "OUT = " << dato_letto[i] << endl << endl;
        
      }
   }

   static const unsigned TEST_SIZE = 4;
   unsigned short in0_test[TEST_SIZE];
   unsigned short in1_test[TEST_SIZE];
   unsigned short in2_test[TEST_SIZE];
   unsigned short sel_test[TEST_SIZE];
   unsigned short out_test[TEST_SIZE];
   unsigned short dato_letto[TEST_SIZE];
   void init_values()
   {
      in0_test[0] = 0;
      in0_test[1] = 1;
      in0_test[2] = 23;
      in0_test[3] = 666;

      in1_test[0] = 1;
      in1_test[1] = 0;
      in1_test[2] = 32;
      in1_test[3] = 50;

      in2_test[0] = 7;
      in2_test[1] = 39;
      in2_test[2] = 2;
      in2_test[3] = 34;

      sel_test[0] = 0;
      sel_test[1] = 1;
      sel_test[2] = 2;
      sel_test[3] = 2;

      for (unsigned i=0;i<TEST_SIZE;i++)
      {
        switch (sel_test[i])
        {
           case 0 : out_test[i]=in0_test[i]; break;
           case 1 : out_test[i]=in1_test[i]; break;
           case 2 : out_test[i]=in2_test[i]; break;
         }
      }
   }
};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "START TEST" << endl << endl;

  sc_start();

  return test.check();
}
