#include <systemc.h>
#include <string>
#include "MUX_2x3.hpp"

using namespace std;

SC_MODULE(TestBench)
{
   sc_signal<sc_uint<3> > ingresso0;
   sc_signal<sc_uint<3> > ingresso1;
   sc_signal<sc_uint<1> >  selezione;
   sc_signal<sc_uint<3> > uscita;
   MUX_2x3 mux;

   SC_CTOR(TestBench) : mux("mux")
   {
        SC_THREAD(stimulus_thread);
        mux.X0(ingresso0);
        mux.X1(ingresso1);
        mux.sel(selezione);
        mux.Y(uscita);
        init_values();
   }

   int check()
   {
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            if (dato_letto[i] != out_test[i])
            {
                cout << "TEST FALLITO: " << i << endl;
                cout << "RISULTATO TEORICO: " << out_test[i] << endl;
                cout << "RISULTATO TEST :" << dato_letto[i] << endl;
                return 1;
             }
        }
        cout << "TEST OK" << endl;                 
        return 0;
   }

   private:

   void stimulus_thread()
   {
      for (unsigned i=0;i<TEST_SIZE;i++) 
      {
        ingresso0.write(in0_test[i]);
        ingresso1.write(in1_test[i]);
        selezione.write(sel_test[i]);
        cout << "IN0 = " << in0_test[i] << endl;
        cout << "IN1 = " << in1_test[i] << endl;
        cout << "SEL = " << sel_test[i] << endl;
        wait(1,SC_NS);
        dato_letto[i]=uscita.read();
        cout << "OUT = " << dato_letto[i] << endl << endl;
        
      }
   }

   static const unsigned TEST_SIZE = 4;
   unsigned short in0_test[TEST_SIZE];
   unsigned short in1_test[TEST_SIZE];
   unsigned short sel_test[TEST_SIZE];
   unsigned short out_test[TEST_SIZE];
   unsigned short dato_letto[TEST_SIZE];
   void init_values()
   {
      in0_test[0] = 0;
      in0_test[1] = 1;
      in0_test[2] = 6;
      in0_test[3] = 5;

      in1_test[0] = 1;
      in1_test[1] = 0;
      in1_test[2] = 2;
      in1_test[3] = 3;

      sel_test[0] = 0;
      sel_test[1] = 1;
      sel_test[2] = 0;
      sel_test[3] = 1;

      for (unsigned i=0;i<TEST_SIZE;i++)
      {
        if (sel_test[i]==0)
           out_test[i]=in0_test[i];
        else
           out_test[i]=in1_test[i];
      }
   }
};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "START TEST" << endl << endl;

  sc_start();

  return test.check();
}
