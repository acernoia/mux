#ifndef MUX_2x16_HPP
#define MUX_2x16_HPP

SC_MODULE(MUX_2x16)
{
   sc_in<sc_uint<16> >  X0;
   sc_in<sc_uint<16> >  X1;
   sc_in<sc_uint<1> >   sel;
   sc_out<sc_uint<16> > Y;

   SC_CTOR(MUX_2x16)
   {
     SC_THREAD(select);
     sensitive << X0 << X1 << sel;
   }
   private:
   void select ();
};


#endif
