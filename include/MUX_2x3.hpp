#ifndef MUX_2x3_HPP
#define MUX_2x3_HPP

SC_MODULE(MUX_2x3)
{
   sc_in<sc_uint<3> >  X0;
   sc_in<sc_uint<3> >  X1;
   sc_in<sc_uint<1> >   sel;
   sc_out<sc_uint<3> > Y;

   SC_CTOR(MUX_2x3)
   {
     SC_THREAD(select);
     sensitive << X0 << X1 << sel;
   }
   private:
   void select ();
};


#endif
