#ifndef MUX_3x16_HPP
#define MUX_3x16_HPP

SC_MODULE(MUX_3x16)
{
   sc_in<sc_uint<16> >  X0;
   sc_in<sc_uint<16> >  X1;
   sc_in<sc_uint<16> >  X2;
   sc_in<sc_uint<2> >   sel;
   sc_out<sc_uint<16> > Y;

   SC_CTOR(MUX_3x16)
   {
     SC_THREAD(select);
     sensitive << X0 << X1 << X2 << sel;
   }
   private:
   void select ();
};


#endif
